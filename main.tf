resource "aws_s3_bucket" "test-bucket-tf2" {
  bucket = "test-bucket-tf2"
  tags = {
    Name        = "My bucket"
    Environment = "dev"
  }
}